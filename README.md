# pipedrive_test

Pipedrive task

## Getting started

Pipedrive Challenge - A Test Task for Pipedrive
This is a project that uses both Github's and Pipedrive's APIs to:

Query a given user's gists
Create a deal in Pipedrive for each of those gists
Periodically check for new gists using a web endpoint

# Part I
It was developed using python, Flask to define interfaces for API methods and vs code as my IDE; We can run the app via two methods but first activate the venv env: source pipedrive-env/bin/acivate and install the dependencies found requirement.txt file.
- export FLASK_APP=app
    flask run
- python project/app.py

we can access the application 127.0.0.1:5000 or localhost:5000

The Github GET method username field accepts a username to retrieve available public gists for that user and also creates a Pipedrive deal for each gist. The username will be saved on lastSeen for periodic checks (every three hours) to see if any new gists have been created since the last visit(this was not properly implemented die to time constraint);

# Part II

The Gitlab CI/CD pipeline was used for this project amd deployed to aws cloud using teraform as the provisioning tool which in my opinion is scalable in this regards.
To deploy the pipeline, run the pipeline file: .gitlab-ci-yml
setting up the pipeline:
Authenticate the Gitlab user with the AWS/IAM user
create the environment variable for 
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS-CONFIG = aws-auth

Access to my root account on aws: indigorex@protonmail.com password: Ogbors135! (will change password after today)

I used terraform as the provisioning tools to EC2 which is scalable and for security the security group should be open to my ip address but was intentioanlly open so easy access during checks.

Run the pipeline and accesss using the AWS_DNS on port 5000/username

Article that help with the project:
Setup env variable: https://docs.gitlab.com/ee/ci/variables/
For authenticating AWS to Gitlab: https://docs.gitlab.com/ee/ci/cloud_deployment/



```
cd existing_repo
git remote add origin https://gitlab.com/devops_projects4/pipedrive_test.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/devops_projects4/pipedrive_test/-/settings/integrations)

Known Issues:
The run gist user and update was to be implented in the entrypoint.sh but due to time constraint.
