#!/bin/sh
echo -e " "
echo -e "\e[92m ----Installing Build Tools----"
echo -e " "
apt update && sudo apt install vault
apk -v --update add python3 py-pip groff ca-certificates curl jq bash openssl gettext git 2>&1 > /dev/null || exit 1
apk --no-cache update && apk add vault
pip install --upgrade awscli 2>&1 > /dev/null || exit 1
wget -q https://releases.hashicorp.com/terraform/0.13.7/terraform_0.13.7_linux_amd64.zip 2>&1 > /dev/null && unzip terraform_0.13.7_linux_amd64.zip 2>&1 > /dev/null && mv terraform /usr/local/bin/ 2>&1 > /dev/null || exit 1
